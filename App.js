// import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Dimensions, Text, View, Image, TouchableWithoutFeedback, SafeAreaView, Button, Alert, Platform, StatusBar, ImageBackground, TextInput, Switch } from 'react-native';
import { useDeviceOrientation } from '@react-native-community/hooks';
import { useEffect, useState } from 'react';
import WelcomeScreen from './app/screen/WelcomeScreen';
import ViewImageScreen from './app/screen/ViewImageScreen';
import CardListing from './app/screen/CardListing';
import ListingDetailsView from './app/screen/ListingDetailsView';
import MessageScreen from './app/screen/MessageScreen';
import Screen from './app/components/Screen';
import Icon from './app/components/Icon';
import ListItem from './app/components/ListItem';
import { GestureHandlerRootView } from 'react-native-gesture-handler';
import AccountScreen from './app/screen/AccountScreen';
import AppInputText from './app/components/AppInputText';
import AppPicker from './app/components/AppPicker';
import LoginScreen from './app/screen/LoginScreen';
import ListingEditScreen from './app/screen/ListingEditScreen';
import ListingScreen from './app/screen/ListingScreen';
import { AppFormField } from './app/components/forms';


export default function App() {
  const [orientation, setOrientation] = useState('portrait');
  const [textInput, setTextInput] = useState('')
  const [isValue, setIsValue] = useState(false)

  useEffect(() => {
    const { width, height } = Dimensions.get('window');
    setOrientation(width > height ? 'landscape' : 'portrait');
  }, []);

  const handleAlert = (event) => {
    event.preventDefault()
    Alert.alert("My title", "My Message", [{ text: "Yes", onPress: () => console.log("Yes") }, { text: "No", onPress: () => console.log("No") }])
  }

  return (
    // <WelcomeScreen />
    // <MessageScreen/>
    // <Screen>
    //   <Icon name="email" size={50} backgroundColor="black" iconColor="white" />
    // </Screen>
    // <GestureHandlerRootView>
    //   <Screen>
    //     <ListItem title="My Title" subtitle="My subtitle" ImageComponent={<Icon name="email" size={50} backgroundColor=""/>} />
    //   </Screen>
    // </GestureHandlerRootView>
    // <GestureHandlerRootView>
    //   <AccountScreen />

    // </GestureHandlerRootView>

    // <Screen>
    //   <AppInputText placeHolder="Email" icon="email" />
    //   <AppPicker/>
    //   <LoginScreen/>
    //   <ListingEditScreen/>
    //   <Switch value={isValue} onValueChange={(value) => { setIsValue(value) }} />
    //    <Text>{textInput}</Text>
    //   <TextInput secureTextEntry placeholder='Testing' style={{borderColor:"#ccc",borderWidth:1,marginTop:10,paddingTop:10}} onChangeText={(text) => setTextInput(text)} /> */}
    // </Screen>

    <Screen>
      <ListingEditScreen/>
    </Screen>

    // <CardListing/>
    // <ListingDetailsView/>
    // <ViewImageScreen/>

    // <GestureHandlerRootView>
    //   <Screen>
    //   {/* <ListItem title="My Title"  subtitle="My subtitle" ImageComponent={<Icon name="email" size={50} backgroundColor="" />} /> */}
    //   <MessageScreen/>
    //   </Screen>
    // </GestureHandlerRootView>

  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    // alignItems: 'center',
    // justifyContent: 'center',
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0
  },
  background: {
    flex: 1,
    resizeMode: "cover",
    height: "100%",
    width: "100%"
  },
  innerContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  logo: {
    top: 150,
    position: "absolute"
  },
  buttonContainer: {
    bottom: 0,
    width: "100%",
    //  height:"auto",
    position: "absolute",
    // height:"auto"
  },
  button: {
    backgroundColor: 'blue',
    paddingVertical: 15,
    paddingHorizontal: 30,
    borderRadius: 10,
    // marginVertical: 10,
  },
  registerButton: {
    backgroundColor: 'green',
    paddingVertical: 15,
    paddingHorizontal: 30,
    borderRadius: 10,
    // marginVertical: 10,
  },
  buttonText: {
    color: 'white',
    fontSize: 18,
    textAlign: "center"
  },
});


//    <SafeAreaView style={styles.container}>

{/* <Button  title="Click Me" onPress={(event)=>{handleAlert(event)}}/> */ }
{/* <StatusBar style="auto" /> */ }
{/* <Text>Open to  on app!</Text>
      <TouchableWithoutFeedback onPress={()=>{console.log("Touchable")}}>
      <Image source={{uri:"https://picsum.photos/200/300",height:300,width:300}}/>
      </TouchableWithoutFeedback> */}
// </SafeAreaView>