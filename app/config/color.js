export default {
  primary: "#fc5c65",
  secondary: "#4ecdc4",
  black: "#000",
  light: "#f8f4f4",
  danger: "#ff5252",
  white: "#fff",
  dark: "#0c0c0c"
};
