import React from 'react'
import Screen from '../components/Screen'
import ListItem from '../components/ListItem'
import { StyleSheet, View, FlatList } from 'react-native'
import color from '../config/color'
import Icon from '../components/Icon'
import ListItemSeparator from '../components/ListItemSeparator'


const menuItems = [
    {
        id: 1,
        title: "My Listings",
        icon: {
            name: "format-list-bulleted",
            backgroundColor: color.primary


        }
    },
    {
        id: 2,
        title: "My Messages",
        icon: {
            name: "format-list-bulleted",
            backgroundColor: color.secondary


        }
    },
]

const AccountScreen = () => {
    return (
        <Screen style={styles.screen} >

            <View style={styles.container}>
                <ListItem title="Shubham" image={require("../assets/mosh.jpg")} subtitle="shubham.vig11@gmail.com" />
            </View>
            <View style={styles.container}>
                <FlatList data={menuItems} keyExtractor={(item) => item.id} ItemSeparatorComponent={ListItemSeparator} renderItem={({ item }) => <ListItem title={item.title} ImageComponent={<Icon name={item.icon.name} size={50} backgroundColor={item.icon.backgroundColor} />} />} />
            </View>
            <ListItem title="Log out" ImageComponent={<Icon name="logout" size={50} backgroundColor="red" />} />

        </Screen>
    )
}

const styles = StyleSheet.create({
    container: {
        marginVertical: 10
    },
    screen: {
        backgroundColor: color.light,
        height: "100%"
    }
})

export default AccountScreen