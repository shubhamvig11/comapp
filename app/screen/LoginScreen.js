import React from 'react';
import { Image, StyleSheet, View, Text } from 'react-native';
import * as Yup from 'yup';



import { AppForm, AppFormField, SubmitButton } from '../components/forms';


const validationSchema = Yup.object().shape({
    email: Yup.string().required().email().label("Email"),
    password: Yup.string().required().min(4).label("Password")
});

const LoginScreen = () => {
    return (
        <View style={styles.container}>
            <Image source={require('../assets/logo-red.png')} style={styles.logo} />
            <AppForm
                initialValues={{ email: "", password: "" }}
                onSubmit={values => console.log(values)}
                validationSchema={validationSchema}
            >
                <AppFormField
                    placeHolder="Email"
                    icon="email"
                    autoCapitalize="none"
                    name="email"
                    autoCorrect={false}
                    keyboardType="email-address"
                    textContentType="emailAddress"
                />
                <AppFormField
                    placeHolder="Password"
                    icon="lock"
                    name="password"
                    autoCapitalize="none"
                    secureTextEntry
                    autoCorrect={false}
                    textContentType="password"
                />
                <SubmitButton title="Login" />

            </AppForm>
        </View>
    );
};

const styles = StyleSheet.create({
    logo: {
        height: 80,
        width: 80,
        alignSelf: "center",
        marginTop: 50,
        marginBottom: 20
    },
    container: {
        padding: 10
    },
    error: {
        color: "red",
        marginBottom: 10
    }
});

export default LoginScreen;
