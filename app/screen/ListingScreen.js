import React from 'react'
import Screen from '../components/Screen'
import { FlatList, StyleSheet } from 'react-native'
import Card from '../components/Card'
import color from '../config/color'

const listings = [
    {
        id: 1,
        title: "Red Jacket for Sale",
        price: 100,
        image: require('../assets/jacket.jpg')

    },
    {
        id:  2,
        title: "Couch in great condition",
        price: 100,
        image: require('../assets/couch.jpg')

    }
]


const ListingScreen = () => {
    return (
        <Screen style={styles.screen}>
            <FlatList data={listings} keyExtractor={listings => listings.id.toString()}
                renderItem={({ item }) => <Card title={item.title} subtitle={"$" + item.price}
                    image={item.image} />}
            />
        </Screen>
    )
}

const styles = StyleSheet.create({
    screen:{
        padding:20,
        backgroundColor:color.light
    }
})

export default ListingScreen