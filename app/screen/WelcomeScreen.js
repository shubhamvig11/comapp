import React from 'react';
import { ImageBackground, StyleSheet, View, Text, Image } from 'react-native';
import AppLoginButton from '../components/AppLoginButton';
import AppRegisterButton from '../components/AppRegisterButton';
import color from '../config/color';

function WelcomeScreen(props) {
    return (
        <ImageBackground style={styles.background}  source={require('../assets/background.jpg')}>
            <View style={styles.logoContainer}>
                <Image source={require('../assets/logo-red.png')} style={styles.icon} />
                <Text style={styles.text}> Sell What You Don't Need</Text>
            </View>
              <AppLoginButton title='Login'/>
           <AppRegisterButton title='Register'/>
        </ImageBackground>
    );
}

const styles = StyleSheet.create({
    background: {
        flex: 1,
        justifyContent: "flex-end",
        alignItems: "center",
        resizeMode:'cover'
    },
    text: {
        textAlign: "center",
        fontSize: 20,
        color: color.black
    },
    icon: {
        height: 100,
        width: 100,

    },
    logoContainer: {
        position: "absolute",
        top: 150,
        alignItems:"center"
    }
})

export default WelcomeScreen;