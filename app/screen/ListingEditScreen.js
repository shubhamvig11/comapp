import React from 'react'
import { View } from 'react-native'
import AppInputText from '../components/AppInputText'
import AppPicker from '../components/AppPicker'
import * as Yup from 'yup';
import { AppForm, AppFormField, SubmitButton } from '../components/forms';




const ListingEditScreen = () => {


    const validationSchema = Yup.object().shape({
        email: Yup.string().required().email().label("Email"),
        password: Yup.string().required().min(4).label("Password")
    });

    return (
        <View>
            <AppForm
                initialValues={{ email: "", password: "" }}
                onSubmit={values => console.log(values)}
                validationSchema={validationSchema}
            >
                <AppFormField
                    placeHolder="Title"
                    autoCapitalize="none"
                    name="title"
                    autoCorrect={false}
                    keyboardType="string"
                />
                <AppPicker width='50%' />
                <AppFormField
                    placeHolder="Price"
                    autoCapitalize="none"
                    name="price"
                    autoCorrect={false}
                    keyboardType="numeric"
                    width={120}
                />
                <AppFormField
                    placeHolder="Description"
                    autoCapitalize="none"
                    name="description"
                    autoCorrect={false}
                    keyboardType="text"
                />
                <SubmitButton title="Post"/>
            </AppForm>
            {/* <AppInputText placeHolder="Title"/>
            <AppInputText placeHolder="Price" width={120}/>
            <AppPicker/>
            <AppInputText placeHolder="Description"/>
            <AppLoginButton title="Submit"/> */}
        </View>
    )
}

export default ListingEditScreen