import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import Card from '../components/Card'

const CardListing = () => {
    return (
        <View style={styles.cardContainer}>
            <Card title="Red jacket for Sale" subtitle="$100" image={require("../assets/jacket.jpg")}/>
        </View>
    )
}

const styles = StyleSheet.create({
    cardContainer: {
        backgroundColor: "#f8f4f4",
        padding: 20,
        paddingTop: 100
    }

})

export default CardListing