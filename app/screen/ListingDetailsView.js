import React from 'react'
import { Image, View, StyleSheet, Text } from 'react-native'
import color from '../config/color'
import ListItem from '../components/ListItem'

const ListingDetailsView = () => {
    return (
        <View>
            <Image source={require('../assets/jacket.jpg')} style={styles.image} />
            <View style={styles.container}>
                <Text style={styles.title}>Red Jacket For Sale</Text>
                <Text style={styles.subTitle}>
                    $100
                </Text>
                <View style={styles.userContainer}>
                <ListItem image={require("../assets/mosh.jpg")} title="Shubham" subtitle="5 Listings" />

                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    image: {
        width: '100%',
        height: 300
    },
    container: {
        padding: 20
    },
    title: {
        fontSize: 24,
        fontWeight: "500"
    },
    subTitle: {
        color: color.secondary,
        fontWeight: "bold",
        marginVertical: "10"
    },
    userContainer:{
        marginVertical:40
    }
})

export default ListingDetailsView