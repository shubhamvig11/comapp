import React, { useState } from 'react';
import { FlatList, StyleSheet, View } from 'react-native';
import ListItem from '../components/ListItem';
import Screen from '../components/Screen';
import ListItemSeparator from '../components/ListItemSeparator';
import { GestureHandlerRootView } from 'react-native-gesture-handler';
import ListItemDelete from '../components/ListItemDelete';

const initialArray = [
    {
        id: 1,
        title: 'T1',
        description: 'D1',
        image: require('../assets/mosh.jpg')
    },
    {
        id: 2,
        title: 'T2',
        description: 'D2',
        image: require('../assets/mosh.jpg')
    },
];

const MessageScreen = () => {
    const [messages, setMessages] = useState(initialArray);
    const [refreshing, setRefreshing] = useState(false)

    const handleDelete = (itemId) => {
        const updatedMessages = messages.filter((item) => item.id !== itemId);
        setMessages(updatedMessages);
    };

    return (
        <GestureHandlerRootView>
            <Screen>
                <FlatList
                    data={messages}
                    keyExtractor={(item) => item.id.toString()}
                    renderItem={({ item }) => (
                        <ListItem
                            title={item.title}
                            subtitle={item.description}
                            image={item.image}
                            onPress={() => console.log("Message:", item)}
                            renderRightActions={() => (
                                <ListItemDelete onPress={() => handleDelete(item.id)} />
                            )}
                        />
                    )}
                    ItemSeparatorComponent={ListItemSeparator}
                    refreshing={refreshing}
                    onRefresh={()=>{
                        setMessages([{
                            id: 2,
                            title: 'T2',
                            description: 'D2',
                            image: require('../assets/mosh.jpg')
                        }])
                    }}
                />
            </Screen>
        </GestureHandlerRootView>
    );
};

export default MessageScreen;
