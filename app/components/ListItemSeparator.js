import React from 'react'
import { View, StyleSheet } from 'react-native'
import color from '../config/color'

const ListItemSeparator = () => {
    return (
        <View style={styles.screen} />
    )
}

const styles = StyleSheet.create({
    screen: {
        width: "100%",
        height: 1,
        backgroundColor: color.light
    }
})

export default ListItemSeparator