import React from 'react'
import { View, StyleSheet, TouchableWithoutFeedback } from 'react-native'
import color from '../config/color'
import { MaterialIcons } from '@expo/vector-icons';


const ListItemDelete = ({onPress}) => {
    return (
        <TouchableWithoutFeedback onPress={onPress}>
        <View style={styles.screen}>
            <MaterialIcons name="delete" size={24} color={color.white} />
        </View>
        </TouchableWithoutFeedback>
    )
}

const styles = StyleSheet.create({
    screen: {
        backgroundColor: color.danger,
        width: 70,
        alignItems:"center",
        justifyContent:"center"
    }
})

export default ListItemDelete