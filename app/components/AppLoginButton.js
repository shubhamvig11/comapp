import React from 'react'
import { ImageBackground, StyleSheet, View, Text, Image ,TouchableOpacity} from 'react-native';


const AppLoginButton = ({title}) => {
    return (
        <TouchableOpacity style={styles.loginButton}>
            <Text style={styles.loginText}>{title}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    loginText: {
        textAlign: "center",
        fontSize: 20,
        color: "#fff"
    },
    loginButton: {
        width: "90%",
        borderRadius:50,
        height: 60,
        backgroundColor: "#fc5c65",
        justifyContent: "center",
        alignSelf:"center"

    },

})


export default AppLoginButton