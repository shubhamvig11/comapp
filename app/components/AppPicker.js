import React, { useState } from 'react';
import { Picker } from '@react-native-picker/picker';
import { StyleSheet, View, Platform } from 'react-native';
import color from '../config/color';
import CategoryPickerItem from './CategoryPickerItem';

const AppPicker = ({ width }) => {
    const [selectedLanguage, setSelectedLanguage] = useState();

    return (
        <View style={styles.mainContainer}>
            <View style={[styles.container, { width }]}>
                <Picker
                    selectedValue={selectedLanguage}
                    onValueChange={(itemValue) => setSelectedLanguage(itemValue)}
                >
                    {/* <Picker.Item label="JavaScript" value="js" style={styles.pickerItem} />
                    <Picker.Item label="Java" value="jv"style={styles.pickerItem} />
                    <Picker.Item label="C++" value="c++" style={styles.pickerItem} /> */}
                    <CategoryPickerItem/>
                </Picker>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: color.light,
        borderRadius: 25,
        overflow: 'hidden', // Ensure children respect the borderRadius
        paddingHorizontal: 10, // Add padding to avoid clipping of text

    },
    mainContainer: {
        padding: 10
    },
    pickerItem: {
        fontSize: 18,
        fontFamily: Platform.OS === "android" ? "Roboto" : "Avenir",
        color: "grey",
        padding: 15,

    }
});

export default AppPicker;
