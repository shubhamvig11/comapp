import React from 'react'
import { Image, StyleSheet, View, Text } from 'react-native';
import AppInputText from '../AppInputText'
import { useFormikContext } from 'formik'

import { ErrorMessage } from '../../components/forms/index'

const AppFormField = ({ name, width, ...otherProps }) => {

    const { setFieldTouched, handleChange, touched, errors } = useFormikContext()

    return (
        <View style={styles.container}>
            <AppInputText
                onChangeText={handleChange(name)}
                onBlur={() => setFieldTouched(name)}
                {...otherProps}
                width={width}

            />
            <ErrorMessage error={errors[name]} visible={touched[name]} />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        padding: 10
    }
})

export default AppFormField