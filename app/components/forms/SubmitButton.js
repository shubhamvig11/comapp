import React from 'react'
import AppLoginButton from '../AppLoginButton'
import { useFormikContext } from 'formik'

const SubmitButton = ({ title }) => {

    const { handleSubmit } = useFormikContext()

    return (
        <AppLoginButton title={title} onPress={handleSubmit} />

    )
}

export default SubmitButton