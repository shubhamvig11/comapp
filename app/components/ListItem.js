import React from 'react'
import { Text, View, StyleSheet, Image, TouchableHighlight } from 'react-native'
import color from '../config/color'
import Swipeable from 'react-native-gesture-handler/Swipeable';
import { MaterialCommunityIcons } from '@expo/vector-icons';

const ListItem = ({ image, title, subtitle, onPress, ImageComponent, renderRightActions }) => {

    console.log("image",image)

    return (
        <Swipeable renderRightActions={renderRightActions}>
            <TouchableHighlight onPress={onPress} underlayColor={color.light}>
                <View style={styles.container} >
                    {/* <Image source={image} style={styles.image} /> */}
                    {ImageComponent}
                    {image &&
                        <Image source={image} style={styles.image} />
                    }
                    <View style={styles.detailContainer}>
                        <Text style={styles.title}>
                            {title}
                        </Text>
                        {subtitle && <Text style={styles.subTitle} numberOfLines={2}>
                            {subtitle}
                        </Text>}
                        
                    </View>
                    <MaterialCommunityIcons  name="chevron-right" size={25}/>
                </View>
            </TouchableHighlight>
        </Swipeable>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        padding: 15,
        alignItems:"center",
        backgroundColor:color.white
    },
    image: {
        width: 70,
        height: 70,
        borderRadius: 35,
    },
    detailContainer:{
        marginLeft:10,
        flex:1,
        justifyContent:"center"
    },
    title: {
        fontWeight: "500"
    },
    subTitle: {
        color: "#6e6969"
    }

})

export default ListItem