import { MaterialCommunityIcons } from '@expo/vector-icons'
import React from 'react'
import { TextInput, View, StyleSheet, Platform } from 'react-native'
import defaultStyles from '../config/styles'
import color from '../config/color'

const AppInputText = ({ icon, placeHolder, width="100%", ...otherProps }) => {
    return (
        <View style={[styles.container, { width }]}>
            <MaterialCommunityIcons name={icon} size={20} style={styles.icon} />
            <TextInput placeholder={placeHolder} style={defaultStyles.text} {...otherProps} />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: "center",
        flexDirection: "row",
        borderRadius: 25,
        // width: '100%',
        padding: 15,
        marginVertical: 10,
        backgroundColor: color.light
    },
    icon: {
        color: defaultStyles.color.dark,
        marginRight: 10
    },
})

export default AppInputText
