import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import color from '../config/color'


const Card = ({ title, subtitle, image }) => {
    return (
        <View style={styles.container}>
            <Image source={image} style={styles.image} />
            <View style={styles.detailContainer}>
                <Text style={styles.title}>{title}</Text>
                <Text style={styles.subTitle}>{subtitle}</Text>
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        borderRadius: 15,
        backgroundColor: '#fff',
        marginBottom: 20,
        overflow:'hidden'
    },
    image: {
        width: "100%",
        height: 200,

    },
    detailContainer: {
        padding: 20
    },
    title: {
        marginBottom: 7
    },
    subTitle:{
        color:color.secondary
    }

})


export default Card