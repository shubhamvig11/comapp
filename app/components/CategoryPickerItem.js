import React from 'react'
import { StyleSheet, View, Platform } from 'react-native';



const pickerItemCatergory = [
    {
        label: "JavaScript",
        value: "js"
    },
    {
        label: "Java",
        value: "jv"
    },
    {
        label: "C++",
        value: "c++"
    }
]

const CategoryPickerItem = () => {


    return (
        pickerItemCatergory.map((item) => {
            <Picker.Item label={item.label} value={item.value} style={styles.pickerItem} />
        })
    )
}

const styles = StyleSheet.create({
    pickerItem: {
        fontSize: 18,
        fontFamily: Platform.OS === "android" ? "Roboto" : "Avenir",
        color: "grey",
        padding: 15,

    }
})

export default CategoryPickerItem