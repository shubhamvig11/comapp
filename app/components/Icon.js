import React from 'react'
import { View } from 'react-native'
import { Entypo, MaterialCommunityIcons } from '@expo/vector-icons';


const Icon = ({ size, backgroundColor, name, iconColor }) => {
    return (
        <View style={{ width: size, height: size, borderRadius: size / 2, backgroundColor, justifyContent: "center", alignItems: "center" }}>
            {/* <Entypo  /> */}
            <MaterialCommunityIcons name={name} size={24} color="white"/>
        </View>
    )
}

export default Icon