import React from 'react'
import { ImageBackground, StyleSheet, View, Text, Image ,TouchableOpacity} from 'react-native';


const AppRegisterButton = ({title}) => {
  return (
    <TouchableOpacity style={styles.registerButton}>
    <Text style={styles.text}> {title}</Text>
    </TouchableOpacity>

  )
}

const styles = StyleSheet.create({
 
    registerButton: {
        width: "90%",
        borderRadius:50,
        height: 60,
        backgroundColor: "#4ecdc4",
        justifyContent: "center",
        marginTop:15,
        marginBottom:15
    },
    text: {
        textAlign: "center",
        fontSize: 20,
        color: "#fff"
    },

})

export default AppRegisterButton